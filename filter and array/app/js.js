"use strict";



// Метод forEach» используется для перебора массива, о функцию callback для каждого элемента.
// Функции callback передаёт три параметра (item, i, arr):


let names = ['hello', 'world', 23, '23', null];
let type = "string";
 function filterBy (names, type) {
   let newNames = [];
  names.forEach(function (item, i, names) {
    if (typeof item != type) {
      newNames.push(item);
    } 
  });
  return newNames;
};
console.log(filterBy(names, type));