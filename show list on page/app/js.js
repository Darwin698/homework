"use strict";



// Метод forEach» используется для перебора массива, о функцию callback для каждого элемента.
// Функции callback передаёт три параметра (item, i, arr):


let names = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let newNames = names.map(function(item) {
  return item ="<li>"+item+"</li>";
});

function addList (newNames, place = document.body) {
  let newUl = document.createElement("ul");
  for (let index = 0; index < newNames.length; index++) {
    const element = newNames[index];
    newUl.innerHTML += element;
  } 
  place.appendChild(newUl);
};