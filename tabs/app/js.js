"use strict";

function tabstitle(elem) {
  let i = document.getElementsByClassName("active_tabs");
  i[0].classList.add("hide_tabs");
  i[0].classList.remove("active_tabs");
  // let w = document.getElementById("tabs-content_itm");
  elem.classList.add("active_tabs");
  let o =  document.getElementsByClassName("active");
  o[0].classList.add("hide");
  o[0].classList.remove("active");
  let f = document.querySelector("." + elem.id);
  f.classList.remove("hide");
  f.classList.add("active");
};