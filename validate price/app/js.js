"use strict";

// обработчик событие – это сигнал от браузера о том, что что-то произошло на HTML странице


input.onfocus = function () { 
  this.classList.add("agree");
  let elem = document.querySelector("#crnt_prc");
  if (elem){
  elem.remove();
  this.classList.remove('txt_inpt_clr');
  };
  let element = document.querySelector("#wrong_prc");
  if (element){
  element.remove();
  }
  if (this.classList.contains('error')) {
    this.classList.remove('error');
  };
};
input.onblur = function () {
  if (this.value < 0) {
    this.classList.add("error");
    let error = document.createElement("span");
    error.setAttribute("id", "wrong_prc");
    error.innerHTML = "<br> Please enter correct price";
    document.body.append(error);

  } else {
    let tag = document.createElement("span");
    tag.setAttribute("id", "crnt_prc");
    tag.innerHTML = "Текущая цена:" + this.value + '<input id="btn_x" type="button" value="x" onclick="btnDelete()"> <br>';
    document.body.prepend(tag);
    this.classList.add("txt_inpt_clr");
    if (this.classList.contains('agree')) {
      this.classList.remove('agree');
    };
  };
};
function btnDelete() {
  let elem = document.querySelector("#crnt_prc");
  elem.remove();
  input.classList.remove('txt_inpt_clr');
  input.value = "";
}