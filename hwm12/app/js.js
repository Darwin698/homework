"use strict";





// экранирование используют для того чтобы символ который в языке программирования имеет определенную функциональность мог использоваться, как обычный стринговый символ, через / 

function createNewUser() {
  let userName = {
    "firstName": "",
    "lastName": "",
    "birthday": "",
    getLogin: function () {
      let login = this.firstName.substring(0, 1).toLocaleUpperCase() + this.lastName.toLocaleLowerCase();
      return login
    },
    getAge: function () {
      let todayDate = new Date();
      let parts = this.birthday.split('.');
      let birthDate = new Date(parts[1] +"."+ parts[0] +"."+ parts[2]);
      let age = todayDate.getFullYear() - birthDate.getFullYear();
      //let age = new Date(dnSec).getFullYear() - new Date(Date.parse(this.birthday)).getFullYear();
      return age;
    },
    getPassword: function () {
      let password = this.getLogin() + this.getAge();
      console.log(password);
      return password;
    }
  }
  userName.firstName = prompt("Ведите ваше имя");
  userName.lastName = prompt("Ведите вашу фамилию");
  userName.birthday = prompt("Ведите Вашу дату рождения в формате dd.mm.yyyy");
  return userName
}

let res = createNewUser();
res.getPassword()