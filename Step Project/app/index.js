$(document).ready(function () {
    $(".services_item").click(function () {
        $(".is_active_services").removeClass("is_active_services");
        $(".showing_srv").addClass("hidden_srv");
        $(".showing_srv").removeClass("showing_srv");
        $(this).addClass("is_active_services");
        $("." + this.id).addClass("showing_srv")
    });


    $(".amazing_work_menu_item").click(function () {
        $(".amazing_work_item").addClass("hidden_amz_wrk_itm");
        $("." + this.id).removeClass("hidden_amz_wrk_itm");
    });


    $(".amz_wrk_mn_itm_all").click(function () {
        $(".hidden_amz_wrk_itm").removeClass("hidden_amz_wrk_itm");
    });


    $("#btn_load_more").click(function () {
        if (this.classList.contains("btn_load_more")) {

            $(".amz_wrk_lst_hidden").removeClass("amz_wrk_lst_hidden");
            $("#btn_load_more").addClass("scnd_load")
            $("#btn_load_more").removeClass("btn_load_more")
        } else {
            $(".amz_wrk_2nd_hidden").removeClass("amz_wrk_2nd_hidden")
            $(this).hide();
        }
    });
    $(".amazing_work_item").mouseover(function () {
        $(this).hide();
        $(this).after($("<li id=\"amazing_work_item_othrsd\" style=\"list-style: none;\" > <div class=\"amazing_work_item_othrsd\"><div class =\"img_otrsd\"><a href=\"#\"><svg width=\"43\" height=\"42\" viewBox=\"0 0 43 42\" fill=\"none\"xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"><rect x=\"1\" y=\"1\" width=\"41\" height=\"40\" rx=\"20\" stroke=\"#18CFAB\" \/><\/svg><\/a><a href=\"#\"><svg width=\"41\" height=\"41\" viewBox=\"0 0 41 41\" fill=\"none\" xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"><path fill-rule=\"evenodd\" clip-rule=\"evenodd\"d=\"M20.5973 0\.997952C31\.8653 0\.997952 40\.9999 9\.95227 40\.9999 20\.9979C40.9999 32\.0432 31\.8653 40\.9979 20\.5973 40\.9979C9\.3292 40\.9979 0\.194626 32\.0432 0\.194626 20\.9979C0\.194626 9\.95227 9\.3292 0\.997952 20\.5973 0\.997952Z\"fill=\"#18CFAB\" \/><\/svg><\/a><\/div><h1 class=\"amazing_work_item_othrsd_tttl\">creative design</h1><p class=\"amazing_work_item_othrsd_p\">Web Design</p></div></li>"));
    })
    $(".amazing_work_item").mouseout(function () {
        $("#amazing_work_item_othrsd").remove();
        $(this).show();
    });

    $("[id^=cmnt_btn]").click(function () {
        let i;
        if (this.id=="cmnt_btn_rght") {
            i = $(".cmnt_hm_pht_anm").next();
        } else {
            i = $(".cmnt_hm_pht_anm").prev();
        }
        if (i.length != 0) {
            $(".cmnt_hm_pht_anm").removeClass("cmnt_hm_pht_anm");
            i.addClass("cmnt_hm_pht_anm");
            $(".cmnt_active").hide(800);
            $(".cmnt_active").addClass("cmnt_hm_hide");
            $(".cmnt_active").removeClass("cmnt_active");
            $("." + i.attr("id")).show(800);
            $("." + i.attr("id")).addClass("cmnt_active");
            $("." + i.attr("id")).css('display', 'flex');
        } 
    });


});